
## Solution

- Java 17 + Spring Boot + Maven was selected as tech stack
- For simplicity, an in-memory BiMap structure was used as a mock database. It gives 1:1 relation get operations with constant time (original -> short and short -> original).
- 7 char Base62 short url encoding was selected. It can support 100 million URL generation per day for almost 100 years
(10+26 * 2)over7 ~ 100M * 365 * 100
- If a short url is generated, it is mapped with the original form on DB and this mapping is used to get long/short form afterwards.
- A layered architecture was used : Controller-Service-Repository.
- Null/empty urls and invalid urls are validated in Controller layer
- ExceptionHandlerAdvice approach was used to handle exceptions and convert them into related HTTP error codes, as a common practice.
- High coverage unit tests and an integration test was provided for all layers.

### Things to improve and constraints

- Since this is an experimental project, an in-memory DB was used for simplicity. In a real system better to have a key-value noSQL db.
- Since an in-memory DB was provided in the solution, no caching was used. But in a real system a distributed cache layer (like Redis) should exist in front-of the DB operation to handle same URL requests in a short time.