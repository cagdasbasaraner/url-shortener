package com.dkb.urlshortener.service;

import com.dkb.urlshortener.exception.UrlNotFoundException;
import com.dkb.urlshortener.repository.UrlShortenerRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.mockito.Mockito.*;

public class UrlShortenerServiceImplTest {

    private UrlShortenerRepository repository;

    private UrlShortenerService urlShortenerService;

    @BeforeEach
    public void setUp() {
        repository = mock(UrlShortenerRepository.class);
        urlShortenerService = new UrlShortenerServiceImpl(repository);
    }

    @Test
    public void testShorten() {
        String randomUrl = UUID.randomUUID().toString();
        urlShortenerService.shorten(randomUrl);
        verify(repository).getShort(randomUrl);
    }

    @Test
    public void testShorten_NullResult() {
        String randomUrl = UUID.randomUUID().toString();
        when(repository.getShort(randomUrl)).thenReturn(null);
        urlShortenerService.shorten(randomUrl);
        verify(repository).getShort(randomUrl);
    }

    @Test
    public void testResolve() throws UrlNotFoundException {
        String randomUrl = UUID.randomUUID().toString();
        when(repository.getOriginal(randomUrl)).thenReturn(UUID.randomUUID().toString());
        urlShortenerService.resolve(randomUrl);
        verify(repository).getOriginal(randomUrl);
    }

    @Test
    public void testResolve_NullResult() {
        String randomUrl = UUID.randomUUID().toString();
        when(repository.getOriginal(randomUrl)).thenReturn(null);
        Assertions.assertThrows(UrlNotFoundException.class, () -> urlShortenerService.resolve(randomUrl));
    }
}
