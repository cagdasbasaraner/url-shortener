package com.dkb.urlshortener.controller;

import com.dkb.urlshortener.exception.UrlNotFoundException;
import com.dkb.urlshortener.exception.UrlNotValidException;
import com.dkb.urlshortener.payload.UrlPayload;
import com.dkb.urlshortener.service.UrlShortenerService;
import com.dkb.urlshortener.validator.UrlShortenerValidator;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.UUID;

import static org.mockito.Mockito.*;

@SpringBootTest
@AutoConfigureMockMvc
public class UrlShortenerControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private UrlShortenerService urlShortenerService;

    @MockBean
    private UrlShortenerValidator urlShortenerValidator;

    @Test
    public void testShorten_NullUrl() throws Exception {

        UrlPayload payload = new UrlPayload();
        payload.setUrl(null);

        mvc.perform(MockMvcRequestBuilders
                        .post("/v1/urls/short")
                        .content(new ObjectMapper().writeValueAsString(payload))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testResolve_EmptyUrl() throws Exception {

        UrlPayload payload = new UrlPayload();
        payload.setUrl("");

        mvc.perform(MockMvcRequestBuilders
                        .post("/v1/urls/original")
                        .content(new ObjectMapper().writeValueAsString(payload))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testShorten() throws Exception, UrlNotValidException {

        UrlPayload payload = new UrlPayload();
        payload.setUrl("https://www.test.com/" + UUID.randomUUID());

        mvc.perform(MockMvcRequestBuilders
                        .post("/v1/urls/short")
                        .content(new ObjectMapper().writeValueAsString(payload))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());

        verify(urlShortenerValidator).checkUrlValidity(payload.getUrl());
        verify(urlShortenerService).shorten(payload.getUrl());
    }

    @Test
    public void testResolve() throws Exception, UrlNotFoundException {

        UrlPayload payload = new UrlPayload();
        payload.setUrl("https://www.dkb.tinyurl/" + UUID.randomUUID());

        mvc.perform(MockMvcRequestBuilders
                        .post("/v1/urls/original")
                        .content(new ObjectMapper().writeValueAsString(payload))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());

        verify(urlShortenerService).resolve(payload.getUrl());
    }
}
