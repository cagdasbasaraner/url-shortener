package com.dkb.urlshortener.repository;

import com.dkb.urlshortener.entity.UrlPair;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.UUID;

public class UrlShortenerRepositoryImplTest {

    private UrlShortenerRepositoryImpl repository;

    private UrlPair urlPair;
    @BeforeEach
    public void setUp() {
        repository = new UrlShortenerRepositoryImpl();
        urlPair = new UrlPair(UUID.randomUUID().toString(), UUID.randomUUID().toString());
        repository.addUrlPair(urlPair);
    }

    @Test
    public void testGetShort() {
        Assertions.assertEquals(repository.getShort(urlPair.getOriginal()), urlPair.getShortForm());
    }

    @Test
    public void testGetOriginal() {
        Assertions.assertEquals(repository.getOriginal(urlPair.getShortForm()), urlPair.getOriginal());
    }
}
