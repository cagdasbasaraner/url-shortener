package com.dkb.urlshortener.validator;

import com.dkb.urlshortener.exception.UrlNotValidException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.UUID;

public class UrlShortenerValidatorImplTest {

    private UrlShortenerValidator validator = new UrlShortenerValidatorImpl();

    @Test
    public void checkUrlValidity_ValidUrl() throws UrlNotValidException {
        validator.checkUrlValidity("https://www.sample.com/" + UUID.randomUUID());
    }

    @Test ()
    public void checkUrlValidity_InvalidUrl() {
        Assertions.assertThrows(UrlNotValidException.class, () ->  validator.checkUrlValidity(UUID.randomUUID().toString()));
    }

    @Test
    public void checkShortUrlValidity_ValidUrl() throws UrlNotValidException {
        validator.checkShortUrlValidity("https://www.dkb.tinyurl/" + UUID.randomUUID());
    }

    @Test ()
    public void checkShortUrlValidity_InvalidUrl() {
        Assertions.assertThrows(UrlNotValidException.class, () ->  validator.checkShortUrlValidity(UUID.randomUUID().toString()));
    }
}
