package com.dkb.urlshortener.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.UUID;

public class UrlEncoderTest {

    @Test
    public void testEncodedUrlLength() {

        String testUrl1 = "https://www.abc.com/" + UUID.randomUUID();
        String testUrl2 = "https://www.test.com/" + UUID.randomUUID();

        String shortUrl1 = UrlEncoder.encode(testUrl1);
        Assertions.assertEquals(shortUrl1.length(), UrlEncoder.HOST.length() + UrlEncoder.MAX_CHARS);
        String shortUrl2 = UrlEncoder.encode(testUrl2);
        Assertions.assertEquals(shortUrl2.length(), UrlEncoder.HOST.length() + UrlEncoder.MAX_CHARS);

        Assertions.assertNotEquals(shortUrl1, shortUrl2);
    }
}
