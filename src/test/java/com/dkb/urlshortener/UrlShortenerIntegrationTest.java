package com.dkb.urlshortener;

import com.dkb.urlshortener.payload.UrlPayload;
import com.dkb.urlshortener.payload.UrlResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@AutoConfigureMockMvc
public class UrlShortenerIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Test
    public void testShorten() throws Exception {
        UrlPayload payload = new UrlPayload();
        payload.setUrl("https://www.test.com/" + UUID.randomUUID());

        UrlResponse shorten = shorten(payload);
        UrlResponse shortenSecondTime = shorten(payload);
        assertEquals(shorten.getUrl(), shortenSecondTime.getUrl());

        UrlResponse resolve = resolve(shorten);
        assertEquals(payload.getUrl(), resolve.getUrl());
    }

    private UrlResponse shorten(UrlPayload payload) throws Exception {
        String shortForm = mvc.perform(MockMvcRequestBuilders
                        .post("/v1/urls/short")
                        .content(new ObjectMapper().writeValueAsString(payload))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();

        return new ObjectMapper().readValue(shortForm, UrlResponse.class);
    }

    public UrlResponse resolve(UrlResponse urlResponse) throws Exception {
        UrlPayload shortUrlPayload = new UrlPayload();
        shortUrlPayload.setUrl(urlResponse.getUrl());
        String originalForm = mvc.perform(MockMvcRequestBuilders
                        .post("/v1/urls/original")
                        .content(new ObjectMapper().writeValueAsString(shortUrlPayload))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();

       return new ObjectMapper().readValue(originalForm, UrlResponse.class);
    }
}
