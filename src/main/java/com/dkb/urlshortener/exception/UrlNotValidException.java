package com.dkb.urlshortener.exception;

public class UrlNotValidException extends Exception {
    public UrlNotValidException(String message) {
        super(message);
    }
}
