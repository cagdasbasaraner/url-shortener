package com.dkb.urlshortener.validator;

import com.dkb.urlshortener.exception.UrlNotValidException;

public interface UrlShortenerValidator {

    void checkUrlValidity(String url) throws UrlNotValidException;

    void checkShortUrlValidity(String url) throws UrlNotValidException;
}
