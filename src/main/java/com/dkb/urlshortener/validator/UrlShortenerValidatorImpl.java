package com.dkb.urlshortener.validator;

import com.dkb.urlshortener.exception.UrlNotValidException;
import com.dkb.urlshortener.util.UrlEncoder;
import org.springframework.stereotype.Component;

import java.net.MalformedURLException;
import java.net.URL;

@Component
public class UrlShortenerValidatorImpl implements UrlShortenerValidator {

    public void checkUrlValidity(String url) throws UrlNotValidException {
        try {
            new URL(url);
        } catch (MalformedURLException e) {
            throw new UrlNotValidException(e.getMessage());
        }
    }

    @Override
    public void checkShortUrlValidity(String url) throws UrlNotValidException {
        if (!url.startsWith(UrlEncoder.HOST)) {
            throw new UrlNotValidException("Given short URL is not valid");
        }
    }
}
