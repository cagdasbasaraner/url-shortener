package com.dkb.urlshortener.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class UrlEncoder {

    public static final String HOST = "https://www.dkb.tinyurl/";
    protected static final int MAX_CHARS = 7;

    private static final String alphabet = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private Map<String, String> keyMap = new HashMap<>();
    private static Random rand = new Random();
    public static String encode(String longUrl) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < MAX_CHARS; i++) {
            stringBuilder.append(alphabet.charAt(rand.nextInt(62)));
        }
        return HOST + stringBuilder;
    }
}
