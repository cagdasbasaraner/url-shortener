package com.dkb.urlshortener.controller;

import com.dkb.urlshortener.exception.UrlNotFoundException;
import com.dkb.urlshortener.exception.UrlNotValidException;
import com.dkb.urlshortener.payload.UrlPayload;
import com.dkb.urlshortener.payload.UrlResponse;
import com.dkb.urlshortener.service.UrlShortenerService;
import com.dkb.urlshortener.validator.UrlShortenerValidator;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/urls")
public class UrlShortenerController {

    @Autowired
    private UrlShortenerService urlShortenerService;

    @Autowired
    private UrlShortenerValidator urlShortenerValidator;

    @PostMapping("/short")
    public ResponseEntity shorten(@Valid @RequestBody UrlPayload urlPayload) throws UrlNotValidException {
        urlShortenerValidator.checkUrlValidity(urlPayload.getUrl());
        return ResponseEntity.ok(new UrlResponse(urlShortenerService.shorten(urlPayload.getUrl())));
    }

    @PostMapping("/original")
    public ResponseEntity resolve(@Valid @RequestBody UrlPayload urlPayload) throws UrlNotFoundException, UrlNotValidException {
        urlShortenerValidator.checkShortUrlValidity(urlPayload.getUrl());
        return ResponseEntity.ok(new UrlResponse(urlShortenerService.resolve(urlPayload.getUrl())));
    }
}
