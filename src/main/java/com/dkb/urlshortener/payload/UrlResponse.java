package com.dkb.urlshortener.payload;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class UrlResponse {

    @NotBlank
    private String url;

    public UrlResponse() {}

    public UrlResponse(String url) {
        this.url = url;
    }
}
