package com.dkb.urlshortener.payload;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class UrlPayload {

    @NotBlank
    private String url;
}
