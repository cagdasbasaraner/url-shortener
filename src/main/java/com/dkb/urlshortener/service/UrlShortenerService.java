package com.dkb.urlshortener.service;

import com.dkb.urlshortener.exception.UrlNotFoundException;

public interface UrlShortenerService {

    String shorten(String original);

    String resolve(String shortForm) throws UrlNotFoundException;
}
