package com.dkb.urlshortener.service;

import com.dkb.urlshortener.entity.UrlPair;
import com.dkb.urlshortener.exception.UrlNotFoundException;
import com.dkb.urlshortener.repository.UrlShortenerRepository;
import com.dkb.urlshortener.util.UrlEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UrlShortenerServiceImpl implements UrlShortenerService {


    private UrlShortenerRepository repository;

    @Autowired
    public UrlShortenerServiceImpl(UrlShortenerRepository repository) {
        this.repository = repository;
    }

    @Override
    public String shorten(String original) {
        String shortForm = repository.getShort(original);
        if (shortForm == null) {
            shortForm = UrlEncoder.encode(original);
            repository.addUrlPair(new UrlPair(original, shortForm));
        }
        return shortForm;
    }

    @Override
    public String resolve(String shortForm) throws UrlNotFoundException {
        String original = repository.getOriginal(shortForm);
        if (original == null) {
            throw new UrlNotFoundException(shortForm + " not found");
        }
        return original;
    }
}
