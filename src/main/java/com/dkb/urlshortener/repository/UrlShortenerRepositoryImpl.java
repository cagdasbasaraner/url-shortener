package com.dkb.urlshortener.repository;

import com.dkb.urlshortener.entity.UrlPair;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import org.springframework.stereotype.Component;

@Component
public class UrlShortenerRepositoryImpl implements UrlShortenerRepository {

    // dummy in-memory db solution : bi-directional map
    private BiMap<String, String> urlPairs = HashBiMap.create();

    public void addUrlPair(UrlPair urlPair) {
        urlPairs.put(urlPair.getOriginal(), urlPair.getShortForm());
    }

    public String getShort(String original) {
        return urlPairs.get(original);
    }

    public String getOriginal(String shortForm) {
        return urlPairs.inverse().get(shortForm);
    }
}
