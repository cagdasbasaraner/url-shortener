package com.dkb.urlshortener.repository;

import com.dkb.urlshortener.entity.UrlPair;

public interface UrlShortenerRepository {

    void addUrlPair(UrlPair urlPair);

    String getShort(String original);

    String getOriginal(String shortForm);
}
