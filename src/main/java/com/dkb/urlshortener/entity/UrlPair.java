package com.dkb.urlshortener.entity;

import lombok.Data;

@Data
public class UrlPair {

    private String original;
    private String shortForm;

    public UrlPair(String original, String shortForm) {
        this.original = original;
        this.shortForm = shortForm;
    }
}
